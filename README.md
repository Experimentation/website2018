# Website2018

> This personal website is created to show my programming capabilities using many development languages and techniques.

- It leverages Angular for DOM binding, routing as well as performing HTTP Get requests to API Gateway.

- The Demo API (https://api.justfortest.com/iotdht22reading) has only one Get method which interact with AWS Lambda to retrieve latest IoT record (room temperature and humidity) from DynamoDB.
