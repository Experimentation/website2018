import { Component, OnInit } from '@angular/core';
// import { slideInAnimation } from './app.animation';


@Component({
  selector: 'fs-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Fady\'s Website';
  today = Date.now();

constructor() {
}
}
