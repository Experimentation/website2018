import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './home/welcome.component';
import { Dht22IotReadingComponent } from './dht22IoT/dht22-iot-reading.component';

const routes: Routes = [
  {path: 'home', component: WelcomeComponent},
  {path: 'dht22IoT', component: Dht22IotReadingComponent},
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: '**', redirectTo: 'home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
