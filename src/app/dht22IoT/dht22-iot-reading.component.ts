import { Component, OnInit } from '@angular/core';
import { Dht22IotReadingService } from './dht22-iot-reading.service';
import { IPayload } from './dht-reading';

@Component({
  templateUrl: './dht22-iot-reading.component.html',
  styleUrls: ['./dht22-iot-reading.component.css']
})
export class Dht22IotReadingComponent implements OnInit {

  dht22IotReadingPayload: IPayload;
  bgPerfectClass = 'bg-perfect';
  bgHotClass = 'bg-hot';
  bgColdClass = 'bg-cold';
  ComfortIndicatorTooltip: string = '';
  hotThreshold = 78;
  coldThreshold = 68;
  loading = true;
  errorMessage: string;

  constructor(private dht22IotReadingService: Dht22IotReadingService) { }

  ngOnInit() {
    this.dht22IotReadingService.getLatestReading().subscribe(
      payload => {
        this.dht22IotReadingPayload = payload;
        this.setComfortIndicator();
        this.loading = false;
      },
      error => {
        this.errorMessage = <any>error;
        this.loading = false;
      }
    );
  }
  private setComfortIndicator() {
    const temperature: number = Number(this.dht22IotReadingPayload.Temperature);

    if (temperature <= this.coldThreshold) {
      this.bgColdClass = 'bg-cold-active';
      this.ComfortIndicatorTooltip = 'Currently it feels Cold!!';
      this.bgHotClass = 'bg-hot';
      this.bgPerfectClass = 'bg-perfect';
    } else if (temperature > this.coldThreshold && temperature <= this.hotThreshold) {
      this.bgColdClass = 'bg-cold';
      this.bgHotClass = 'bg-hot';
      this.bgPerfectClass = 'bg-perfect-active';
      this.ComfortIndicatorTooltip = 'Currently it feels Awesome!!';
    } else {
      this.bgColdClass = 'bg-cold';
      this.bgHotClass = 'bg-hot-active';
      this.ComfortIndicatorTooltip = 'Currently it feels Hot!!';
      this.bgPerfectClass = 'bg-perfect';
    }
  }
 public onInput(event: any) {
   if (this.dht22IotReadingPayload) {
      this.setComfortIndicator();
   }
 }
}
