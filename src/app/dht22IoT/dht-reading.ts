export interface IPayload {
  Temperature: string;
  Time: string;
  SessionId: string;
  Humidity: string;
  DeviceName: string;
  DHT22Status: string;
}
