import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import {IPayload} from './dht-reading';

@Injectable({
  providedIn: 'root'
})
export class Dht22IotReadingService {
  private productUrl = 'https://api.justfortest.com/iotdht22reading'; // 'api/payload.json';

  constructor(private http: HttpClient) {}

  getLatestReading(): Observable<IPayload> {
    return this.http.get<IPayload>(this.productUrl).pipe(
      tap(data => console.log('ServerResponse: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  private handleError(err: HttpErrorResponse) {
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }
}
